from tornado import gen, ioloop, web, websocket
from tornado.options import define, options
from datetime import datetime
from pytz import timezone
from os import path
import workcalypse.countdown as countdown


class Publisher(object):
    def __init__(self):
        self.subscribers = set()
        self.started = False

    def register(self, subscriber):
        self.subscribers.add(subscriber)

    def deregister(self, subscriber):
        self.subscribers.remove(subscriber)

    @gen.coroutine
    def start(self):
        while True:
            yield gen.sleep(1)
            if len(self.subscribers) > 0:
                free = [datetime(2018, 2, 28),
                        datetime(2018, 3, 1),
                        datetime(2018, 3, 2),

                        datetime(2018, 3, 7),
                        datetime(2018, 3, 8),
                        datetime(2018, 3, 9),
                        datetime(2018, 3, 12),
                        datetime(2018, 3, 13),
                        datetime(2018, 3, 14),

                        datetime(2018, 3, 15),
                        datetime(2018, 3, 16),
                        datetime(2018, 3, 19),
                        datetime(2018, 3, 20),
                        datetime(2018, 3, 21),
                        datetime(2018, 3, 22),
                        datetime(2018, 3, 23),
                        datetime(2018, 3, 26),
                        datetime(2018, 3, 27),
                        datetime(2018, 3, 28),
                        datetime(2018, 3, 29),
                        datetime(2018, 3, 30)]
                now = datetime.now(timezone('Europe/Berlin'))
                end = datetime(2018, 3, 31)
                rest = countdown.workcalypse(now, end, free)
                message = countdown.response_format(rest)

                yield [subscriber.submit(message) for subscriber in self.subscribers]


class SalvationHandler(websocket.WebSocketHandler):

    def initialize(self, publisher):
        self.publisher = publisher

    def open(self):
        self.publisher.register(self)

    def on_close(self):
        self.publisher.deregister(self)

    def data_received(self, chunk):
        pass

    def on_message(self, message):
        pass

    def submit(self, message):
        self.write_message(message)


def make_app():
    publisher = Publisher()
    publisher.start()
    static_path = path.join(path.dirname(__file__), "static")
    return web.Application([
        (r'/salvation', SalvationHandler, {"publisher": publisher}),
        (r'/(.*)', web.StaticFileHandler, {"path": static_path, "default_filename": "index.html"}),
    ])


define("port", default="8080")
define("host", default="0.0.0.0")

if __name__ == "__main__":
    options.parse_command_line()
    app = make_app()
    app.listen(options.port, options.host)
    ioloop.IOLoop.current().start()
