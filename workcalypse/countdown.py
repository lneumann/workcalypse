from typing import List
from datetime import datetime, timedelta, time


def _working_days(start: datetime, end: datetime, free_days: List[datetime] = []) -> List[datetime]:
    delta = end - start
    days = [start + timedelta(days=d) for d in range(delta.days + 1)]
    days = list(filter(lambda d: d not in free_days, days))
    days = list(filter(lambda d: d.weekday() < 5, days))
    return days[:1] if len(days) == 2 and days[0] == days[1] else days


def _rest_of_day(day: datetime, now: time = datetime.now().time()) -> timedelta:
    now = day.replace(hour=now.hour, minute=now.minute, second=now.second, microsecond=now.microsecond)
    if now.hour < 8:
        return timedelta(days=1)
    elif now.hour >= 17:
        return timedelta(0)
    else:
        day = day.replace(hour=17, minute=0, second=0, microsecond=0)
    return day - now


def workcalypse(now: datetime, end: datetime, free_days: List[datetime] = []) -> timedelta:
    today = datetime(now.year, now.month, now.day)
    days = _working_days(today, end, free_days)
    if len(days) == 0:
        return timedelta(0)
    elif days[0] == today:
        return _rest_of_day(today, now.time()) + timedelta(days=len(days)-1)
    else:
        return timedelta(days=len(days))


def response_format(td: timedelta):
    days = td.days
    hours, minutes_r = divmod(td.seconds, 3600)
    minutes, seconds = divmod(minutes_r, 60)
    seconds += td.microseconds / 1e6
    seconds = int(seconds)

    return {
        "days": days,
        "hours": hours,
        "minutes": minutes,
        "seconds": seconds
    }
