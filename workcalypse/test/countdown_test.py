import unittest
from datetime import datetime, timedelta, time
from pytz import timezone
from workcalypse import countdown


class ServerTest(unittest.TestCase):

    def test_working_days_during_week(self):
        start = datetime(2018, 1, 1)
        end = datetime(2018, 1, 3)
        actual = countdown._working_days(start, end)
        expected = [start, datetime(2018, 1, 2), end]
        self.assertListEqual(actual, expected)

    def test_working_days_one_day(self):
        start = datetime(2018, 1, 1)
        end = datetime(2018, 1, 1)
        actual = countdown._working_days(start, end)
        expected = [start]
        self.assertListEqual(actual, expected)

    def test_working_days_week_with_free_days(self):
        start = datetime(2018, 1, 1)
        end = datetime(2018, 1, 7)
        actual = countdown._working_days(start, end, [datetime(2018, 1, 2), datetime(2018, 1, 4)])
        expected = [start, datetime(2018, 1, 3), datetime(2018, 1, 5)]
        self.assertListEqual(actual, expected)

    def test_working_days_week_with_free_days_today(self):
        start = datetime(2018, 1, 1)
        end = datetime(2018, 1, 4)
        actual = countdown._working_days(start, end, [datetime(2018, 1, 1), datetime(2018, 1, 2)])
        expected = [datetime(2018, 1, 3), datetime(2018, 1, 4)]
        self.assertListEqual(actual, expected)

    def test_working_days_weekend(self):
        start = datetime(2018, 1, 6)
        end = datetime(2018, 1, 7)
        actual = countdown._working_days(start, end)
        expected = []
        self.assertListEqual(actual, expected)

    def test_rest_of_day_at7(self):
        day = datetime(2018, 1, 1)
        now = datetime(2018, 1, 1, 10)
        actual = countdown._rest_of_day(day, now)
        expected = timedelta(hours=7)

        self.assertEqual(actual, expected)

    def test_rest_of_day_at1630(self):
        day = datetime(2018, 1, 1)
        now = time(16, 30)
        actual = countdown._rest_of_day(day, now)
        expected = timedelta(minutes=30)

        self.assertEqual(actual, expected)

    def test_rest_of_day_at1700(self):
        day = datetime(2018, 1, 1)
        now = datetime(2018, 1, 1, 17)
        actual = countdown._rest_of_day(day, now)
        expected = timedelta(0)

        self.assertEqual(actual, expected)

    def test_rest_of_day_at1730(self):
        day = datetime(2018, 1, 1)
        now = datetime(2018, 1, 1, 17, 30)
        actual = countdown._rest_of_day(day, now)
        expected = timedelta(0)

        self.assertEqual(actual, expected)

    def test_rest_of_day_timezoned(self):
        day = datetime(2018, 1, 1)
        now = datetime(2018, 1, 1, 16, 30, tzinfo=timezone("US/Pacific"))
        actual = countdown._rest_of_day(day, now)
        expected = timedelta(minutes=30)

        self.assertEqual(actual, expected)

    def test_workcalypse_week_with_freedays_and_rest(self):
        end = datetime(2018, 1, 8)
        now = datetime(2018, 1, 1, 16, 30)
        free_days = [datetime(2018, 1, 2), datetime(2018, 1, 3)]
        actual = countdown.workcalypse(now, end, free_days)
        expected = timedelta(days=3, minutes=30)

        self.assertEqual(actual, expected)

    def test_workcalypse_no_days_left(self):
        end = datetime(2018, 1, 1)
        now = datetime(2018, 1, 2)
        actual = countdown.workcalypse(now, end)
        expected = timedelta(0)

        self.assertEqual(actual, expected)

    def test_workcalypse_minute_left(self):
        end = datetime(2018, 1, 1)
        now = datetime(2018, 1, 1, 16, 59)
        actual = countdown.workcalypse(now, end)
        expected = timedelta(minutes=1)

        self.assertEqual(actual, expected)

    def test_workcalypse_full_days(self):
        end = datetime(2018, 1, 2)
        now = datetime(2018, 1, 1, 7, 59)
        actual = countdown.workcalypse(now, end)
        expected = timedelta(days=2)

        self.assertEqual(actual, expected)

    def test_response_format(self):
        td = timedelta(days=11, hours=12, minutes=13, seconds=14)
        actual = countdown.response_format(td)
        expected = {"days": 11, "hours": 12, "minutes": 13, "seconds": 14}

        self.assertEqual(actual, expected)

    def test_response_format_withZeroHour(self):
        td = timedelta(days=1, minutes=12, seconds=14)
        actual = countdown.response_format(td)
        expected = {"days": 1, "hours": 0, "minutes": 12, "seconds": 14}

        self.assertEqual(actual, expected)