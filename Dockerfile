FROM python:3.6-alpine

EXPOSE 8080

WORKDIR /app

ADD . /app

RUN pip install -r requirements.txt

ENV PYTHONPATH /app

CMD ["python", "workcalypse/server.py", "--port=8080"]